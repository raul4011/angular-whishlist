import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listadestino',
  templateUrl: './listadestino.component.html',
  styleUrls: ['./listadestino.component.css']
})
export class ListadestinoComponent implements OnInit {
  destinos: string[];
  constructor() { 
    this.destinos = ["Buenos aires","Lima","Barcelona","Montevideo"];
  }

  ngOnInit(): void {
  }

}
