import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadestinoComponent } from './listadestino.component';

describe('ListadestinoComponent', () => {
  let component: ListadestinoComponent;
  let fixture: ComponentFixture<ListadestinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadestinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadestinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
